#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) 2018 Mònica Ramírez Arceda <monica@probeta.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
from data_converter.output_object import OutputObject

class NomenamentNumero(OutputObject):
    def __init__(self):
        self.data = datetime.datetime.now().date()
        self.id_st_dept = ""
        self.nivell = ""
        self.id_espe = ""
        self.numero_borsa = 0
        
    def __str__(self):
        return "[data: " + str(self.data) + \
               ", id_st: " + self.id_st_dept + \
               ", nivell: " + self.nivell + \
               ", id_esp: " + self.id_espe + \
               ", n: " + str(self.numero_borsa) + \
               "]"