#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) 2018 Mònica Ramírez Arceda <monica@probeta.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from data_converter.data_converter import DataConverter
from config import ConfigNomenamentNumero
from nomenament_numero import NomenamentNumero
import os
import logging
from sqlalchemy.orm import mapper
from urllib.request import urlretrieve
from lxml import html
from datetime import datetime

class NomenamentNumeroConverter(DataConverter):
            
    def __init__(self, config_file_path):
        self.config = ConfigNomenamentNumero(config_file_path)
        super(NomenamentNumeroConverter, self).__init__()
        mapper(NomenamentNumero, self.table) 
        logging.basicConfig(filename=self.config.log_file, level=logging.WARNING)
    
    def get_input_files(self):
        urls = open(self.config.urls).readlines()
        for url in urls:
            urlretrieve(url, 
                        os.path.join(
                            self.config.raw_data_dir, 
                            url.split('=')[1].split('&')[0] + url.strip()[-1] + '.html'))
            
    def convert(self):
        # HTML files are readable enough
        pass

    def get_valid_records(self):
        records = []
        today = datetime.now().date()
        for filename in os.listdir(self.config.raw_data_dir):
            # Get st
            st = filename.split('.')[0][0:-1]
            # Get nivell: Primària or Secundària (P or S)
            nivell = filename.split('.')[0][-1]
            # Get html content
            with open(os.path.join(self.config.raw_data_dir, filename), 'rb') as f:
                html_page = f.read()
            tree = html.fromstring(html_page)
            # Get nomenaments date
            data = NomenamentNumeroConverter.get_data_nomenaments(tree).replace('\xa0', ' ')
            if data == '':
                continue
            data = datetime.strptime(data.strip(), '%d/%m/%Y').date()
            if data != '' and data == today:
                if nivell == 'S':
                    especialitats = tree.xpath('//div[@id="fw_colCos"]/table/tr/td[1]/text()')                    
                    convocats = tree.xpath('//div[@id="fw_colCos"]/table/tr/td[2]/text()')     
                    partial_records = NomenamentNumeroConverter.build_records(st, data, 'S', especialitats, convocats)
                    records.extend(partial_records)              
                else:
                    especialitats = tree.xpath('//div[@id="fw_colCos"]/table[1]/tr/td[1]/text()')
                    convocats = tree.xpath('//div[@id="fw_colCos"]/table[1]/tr/td[2]/text()')
                    partial_records = NomenamentNumeroConverter.build_records(st, data, 'P', especialitats, convocats)
                    records.extend(partial_records)
                    especialitats = tree.xpath('//  div[@id="fw_colCos"]/table[2]/tr/td[1]/text()')
                    convocats = tree.xpath('//div[@id="fw_colCos"]/table[2]/tr/td[2]/text()')
                    partial_records = NomenamentNumeroConverter.build_records(st, data, 'PS', especialitats, convocats)
                    records.extend(partial_records)
        return records
                        
    def build_objects(self, raw_records):
        for r in raw_records:
            nn = NomenamentNumero()
            nn.data = r[0]
            nn.id_st_dept = r[1]
            nn.nivell = r[2]
            nn.id_espe = r[3]
            nn.numero_borsa = int(r[4].replace('.',''))
            self.objects.append(nn)
    
    @staticmethod
    def get_data_nomenaments(tree):
        # Get date
        data_nomenaments = tree.xpath('//div[@id="panel_0_0"]/div/div/h4[3]/text()')
        # Ignore HTML entites (blank spaces)
        data_nomenaments = data_nomenaments[0].strip().rstrip()
        # If theres is a date, get it. If not, return ''
        if data_nomenaments != '':
            # String format is Data:dd/mm/yyyy. Get only dd/mm/yyyy
            data_nomenaments = data_nomenaments.split(':')[1]
        return data_nomenaments
    
    @staticmethod
    def build_records(st, data, nivell, especialitats, convocats):
        partial_records = []
        especialitats = NomenamentNumeroConverter.clean_list(especialitats)
        especialitats = [item.split(' ')[1] for item in especialitats]
        convocats = NomenamentNumeroConverter.clean_list(convocats)
        i = 0
        for e in especialitats:
            record = []
            c = convocats[i]
            if c != '' and c != 'No hi ha convocats':
                record.append(data)
                record.append(st)
                record.append(nivell)
                record.append(e.strip())
                record.append(c)
                partial_records.append(record)
            i = i + 1
        return partial_records
    
    @staticmethod
    def clean_list(dirty_list):
        return [item.replace('\xa0', ' ').strip() for item in dirty_list]