DROP TABLE `servei_territorial`;
CREATE TABLE IF NOT EXISTS `servei_territorial` (
  `id` tinyint(4) NOT NULL,
  `id_dept` varchar(4) NOT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `nom_curt` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE (id_dept)
);

INSERT INTO `servei_territorial` (`id`, `id_dept`, `nom`, `nom_curt`) VALUES
(1, '0108', 'Consorci d''Educació de Barcelona', 'Ciutat'),
(2, '0208', 'Barcelona Comarques', 'Comarques'),
(3, '0308', 'Baix Llobregat', 'Baix Llob'),
(4, '0408', 'Vallès Occidental', 'Vallès Oc'),
(5, '0508', 'Maresme-Vallès Oriental', 'Mar Vall Or'),
(6, '1060', 'Catalunya Central', 'Cat Central'),
(17, '0117', 'Girona', 'Girona'),
(25, '0125', 'Lleida', 'Lleida'),
(43, '0143', 'Tarragona', 'Tarragona'),
(44, '0243', 'Terres de l''Ebre', 'Terres Ebre');

CREATE TABLE IF NOT EXISTS `nomenament_numero` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` DATE NOT NULL,
  `id_st` varchar(4) NOT NULL,
  `nivell`varchar(2) NOT NULL,
  `id_esp` varchar(5) NOT NULL,
  `n` INT NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_st`) REFERENCES servei_territorial(`id_dept`)
);

ALTER TABLE `nomenament_numero`
  ADD UNIQUE `unique_index`(`data`, `id_st`, `nivell`, `id_esp`);

