#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) 2017 Mònica Ramírez Arceda <monica@probeta.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os

from nomenament_numero_converter import NomenamentNumeroConverter

parser = argparse.ArgumentParser()
# -l: do not download data. use local data already present
parser.add_argument("-l", "--localdata", action='store_true')
args = parser.parse_args()

xc = NomenamentNumeroConverter(os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'config'))

if not args.localdata:
    print("Obtenint dades")
    xc.get_data()
print("Convertint dades a un format llegible")
xc.convert()
print("Extraient dades")
xc.parse()
print("Exportant a la base de dades")
xc.export()
print("Fi")